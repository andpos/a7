
import java.util.*;

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {

   private static class Node {
      private byte value;
      private byte bits;
      private byte encodedValue;
      private int frequency = 1;
      private Node root, prev, next;
      private Node[] branches;

      Node() {
      }

      Node(byte value, Node root) {
         this.value = value;
         setParent(root);
      }

      Node(int frequency, Node prev) {
         Node next = prev.next;
         if (next != null) next.prev = this;
         this.next = next;
         prev.next = this;
         this.prev = prev;
         this.frequency = frequency;
      }

      Node(int frequency, Node left, Node right) {
         this.frequency = frequency;
         branches = new Node[] {left, right};
         left.root = this;
         right.root = this;
      }

      private Node getNextRootNode(int frequency) {
         Node root = this.next;
         if (root == null || frequency < root.frequency) {
            return new Node(frequency, this);
         }
         else if (frequency > root.frequency) {
            return root.getNextRootNode(frequency);
         }
         return root;
      }

      private void createInternalNode(Node prev, Node curr) {
         int frequency = prev.frequency + curr.frequency;
         Node parent = getNextRootNode(frequency);
         Node internal = new Node(frequency, prev, curr);
         internal.setParent(parent);
      }

      private void increaseFrequency() {
         ++frequency;
         Node root = this.root.next;
         if (root == null || root.frequency != frequency) {
            if (prev == null && next == null && frequency > 2) {
               ++this.root.frequency;
               return;
            }
            root = new Node(frequency, this.root);
         }
         if (prev == null) {
            this.root.root = next;
            if (next == null) {
               this.root.remove();
            }
         }
         else prev.next = next;
         if (next != null) next.prev = prev;
         setParent(root);
      }

      private void remove() {
         if (prev != null) prev.next = next;
         if (next != null) next.prev = prev;
      }

      private void setParent(Node root) {
         this.root = root;
         prev = null;
         next = root.root;
         if (next != null) next.prev = this;
         root.root = this;
      }

      private void encodeValues(int depth, int value) {
         if (branches == null) {
            bits = depth == 0 ? 1 : (byte)depth;
            encodedValue = (byte)value;
         } else {
            branches[0].encodeValues(depth + 1, value << 1);
            branches[1].encodeValues(depth + 1, (value << 1) | 1);
         }
      }
   }

   // TODO!!! Your instance variables here!
   private Node tree;
   private Node[] nodes;
   private int encodedLength;


   /** Constructor to build the Huffman code for a given bytearray.
    * @param original source data
    */
   Huffman(byte[] original) {
      if (original == null) throw new RuntimeException("Source data cannot be null.");

      Node first = new Node();
      nodes = new Node[256];
      for (int i = 0; i < original.length; ++i) {
         byte value = original[i];
         Node node = nodes[value];
         if (node == null)
            nodes[value] = new Node(value, first);
         else
            node.increaseFrequency();
      }

      tree = null;
      Node prev = null;
      Node root = first;
      while (root != null) {
         Node curr = root.root;
         while (curr != null) {
            if (prev == null) {
               prev = curr;
            } else {
               // could remove prev and curr...
               root.createInternalNode(prev, curr);
               prev = null;
            }
            curr = curr.next;
         }
         tree = root;
         root = root.next;
      }
      if (tree.root != null) {
         tree = tree.root;
         tree.encodeValues(0, 0);
      }
   }

   /** Length of encoded data in bits. 
    * @return number of bits
    */
   public int bitLength() {
      return encodedLength;
   }


   /** Encoding the byte array using this prefixcode.
    * @param origData original data
    * @return encoded data
    */
   public byte[] encode(byte[] origData) {
      if (origData == null) throw new RuntimeException("Source data cannot be null.");

      byte[] encodedData = new byte[origData.length];
      byte bitsLeft = 8;
      int currentByte = 0;
      for (int i = 0; i < origData.length; ++i) {
         byte value = origData[i];
         byte bitsNeeded = nodes[value].bits;
         byte encodedValue = nodes[value].encodedValue;

         if (bitsLeft >= bitsNeeded) {
            bitsLeft -= bitsNeeded;
            encodedData[currentByte] |= (encodedValue << bitsLeft);
            if (bitsLeft == 0) {
               bitsLeft = 8;
               ++currentByte;
            }
         } else {
            bitsNeeded -= bitsLeft;
            encodedData[currentByte++] |= (encodedValue >> bitsNeeded);
            bitsLeft = 8;
            bitsLeft -= bitsNeeded;
            encodedData[currentByte] |= (encodedValue << bitsLeft);
         }
      }
      encodedLength = (currentByte + 1) * 8 - bitsLeft;
      return Arrays.copyOf(encodedData, (encodedLength & 7) == 0 ? currentByte : currentByte + 1);
   }

   /** Decoding the byte array using this prefixcode.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
    */
   public byte[] decode (byte[] encodedData) {
      if (encodedData == null) throw new RuntimeException("Source data cannot be null.");

      byte[] decodedData = new byte[encodedData.length * 8];
      int currentByte = 0;

      boolean decoding = true;
      Node curr = tree;
      for (int i = 0; i < encodedData.length && decoding; ++i) {
         byte value = encodedData[i];
         for (int b = 7; b >= 0; --b) {
            if (curr.branches != null) curr = curr.branches[(value >> b) & 1];
            if (curr.branches == null) {
               decodedData[currentByte++] = curr.value;
               curr = tree;
               if ((i + 1) * 8 - b >= encodedLength)
               {
                  decoding = false;
                  break;
               }
            }
         }
      }
      return Arrays.copyOf(decodedData, currentByte);
   }

   /** Main method. */
   public static void main (String[] params) {
      String tekst = "The quick brown fox jumps over the lazy dog";
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman(orig);
      byte[] kood = huf.encode(orig);
      byte[] orig2 = huf.decode(kood);
      // must be equal: orig, orig2
      int length = huf.bitLength();
      System.out.println(Arrays.equals(orig, orig2));
      System.out.println(kood.length);
      System.out.println(tekst);
      System.out.println(new String(orig2));
      System.out.println(orig.length);
      System.out.println(orig2.length);
      System.out.println ("Length of encoded data in bits: " + length);
      // TODO!!! Your tests here!
   }
}
